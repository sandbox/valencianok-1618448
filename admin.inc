<?php

/**
 * Menu callback; Newsletter tab page.
 */
function cc_node_campaign_node_tab_page($node) {
    drupal_set_title(t('<em>Newsletter</em> @title', array('@title' => $node->title)), PASS_THROUGH);
    return drupal_get_form('cc_node_campaign_node_tab_send_form', $node);
}

function cc_node_campaign_admin_form($form, &$form_state) {

    $form['cc_nn_EmailAddress'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Address'),
        '#required' => TRUE,
        '#default_value' => variable_get('cc_nn_EmailAddress'),
    );

    //Build an associated node type list, easy enough.
    $node_type_list = array();
    $query = db_select('node_type', 'nt');
    $query->fields('nt', array('type', 'name'));
    $result = $query->execute();
    foreach ($result as $node_type) {
        $node_type_list[$node_type->type] = $node_type->name;
    }

    $selected_types = variable_get('cc_nn_nodetype', NULL);
    
    //use node_list as options list
    $form['cc_nn_nodetype'] = array(
        '#type' => 'select',
        '#title' => t('Select Node Types'),
        '#options' => $node_type_list,
        '#multiple' => TRUE,
        '#required' => TRUE,
        '#description' => t('Select the node types that will have the CC Campaign tab'),
        '#default_value' => $selected_types,
    );

    $form['cc_nn_OrganizationName'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization Name'),
        '#required' => TRUE,
        '#default_value' => variable_get('cc_nn_OrganizationName'),
    );
    $form['cc_nn_OrganizationAddress1'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization Address1'),
        '#required' => TRUE,
        '#default_value' => variable_get('cc_nn_OrganizationAddress1'),
    );
    $form['cc_nn_OrganizationAddress2'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization Address2'),
        '#default_value' => variable_get('cc_nn_OrganizationAddress2'),
    );
    $form['cc_nn_OrganizationAddress3'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization Address3'),
        '#default_value' => variable_get('cc_nn_OrganizationAddress3'),
    );
    $form['cc_nn_OrganizationCity'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization City'),
        '#required' => TRUE,
        '#default_value' => variable_get('cc_nn_OrganizationCity'),
    );
    $form['cc_nn_OrganizationState'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization State'),
        '#default_value' => variable_get('cc_nn_OrganizationState'),
    );
    $form['cc_nn_OrganizationInternationalState'] = array(
        '#type' => 'textfield',
        '#title' => t('Organization International State'),
        '#default_value' => variable_get('cc_nn_OrganizationInternationalState'),
    );
    $form['cc_nn_OrganizationPostalCode'] = array(
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => t('Organization Postal Code'),
        '#default_value' => variable_get('cc_nn_OrganizationPostalCode'),
    );
    $form['cc_nn_OrganizationCountry'] = array(
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => t('Organization Country (Two letter ISO country codes)'),
        '#default_value' => variable_get('cc_nn_OrganizationCountry'),
    );
    return system_settings_form($form);
}

function cc_node_campaign_node_tab_send_form($form, &$form_state, $node) {
    $form = array();
    // We will need the node
    $form['nid'] = array(
        '#type' => 'value',
        '#value' => $node->nid,
    );

    // @todo delete this fieldset?
    $form['newsletter'] = array(
        '#type' => 'fieldset',
        '#title' => t('Create CC campaign'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
    );

    $form['newsletter']['subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Mail Subject'),
        '#default_value' => $node->title,
    );

    $form['newsletter']['body'] = array(
        '#type' => 'textarea',
        '#title' => t('Mail body'),
        '#default_value' => _cc_node_campaign_get_html($node->nid),
        '#attributes' => array(
            'style' => 'height: 450px; font-size: 12px; font-family: Courier',
        )
    );

    $form['newsletter']['view'] = array(
        '#type' => 'markup',
        '#title' => t('View'),
        '#markup' => l("Preview campaign", "campaign_preview/" . $node->nid),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Create Campaign!'),
    );
    return $form;
}

/**
 * Submit handler for the add contact list admin page
 */
function cc_node_campaign_node_tab_send_form_submit($form, &$form_state) {
    $cc = constant_contact_create_object();
    if (!is_object($cc)):
        return;
    endif;

    $title = (isset($form['newsletter']['subject'])) ? $form['newsletter']['subject']['#value'] : '';

    //Faltaria seleccionar cual lista
    $contact_lists = null; //array(); 
    // las opciones
    $options = array();
    $options['Subject'] = $title;
    $options['FromName'] = variable_get('cc_nn_OrganizationName');
    $options['ViewAsWebpage'] = 'YES';
    $options['ViewAsWebpageLinkText'] = 'Click here';
    $options['ViewAsWebpageText'] = 'Having trouble viewing this email?';
    $options['IncludeForwardEmail'] = 'NO';
    $options['IncludeSubscribeLink'] = 'NO';

    $options['OrganizationName'] = variable_get('cc_nn_OrganizationName');
    $options['OrganizationAddress1'] = variable_get('cc_nn_OrganizationAddress1');
    $options['OrganizationAddress2'] = variable_get('cc_nn_OrganizationAddress2');
    $options['OrganizationAddress3'] = variable_get('cc_nn_OrganizationAddress3');
    $options['OrganizationCity'] = variable_get('cc_nn_OrganizationCity');
    $options['OrganizationState'] = variable_get('cc_nn_OrganizationState');
    $options['OrganizationInternationalState'] = variable_get('cc_nn_OrganizationInternationalState');
    $options['OrganizationPostalCode'] = variable_get('cc_nn_OrganizationPostalCode');
    $options['OrganizationCountry'] = variable_get('cc_nn_OrganizationCountry');
    $options['OrganizationName'] = variable_get('cc_nn_OrganizationName');

    $options['EmailContentFormat'] = 'HTML';
    $options['EmailAddress'] = variable_get('cc_nn_EmailAddress');
    $options['EmailContent'] = str_replace("<", "&lt;", $form['newsletter']['body']['#value']);
    $options['EmailTextContent'] = "&lt;Text>" . strip_tags($form['newsletter']['body']['#value']) . "&lt;/Text>";

    $status = $cc->create_campaign($title, $contact_lists, $options);

    if ($status):
        drupal_set_message(t('A new campaign has been created'));
    else:
        drupal_set_message(t("Failed to create new campaign: {$cc->last_error}"), 'error');
        if ($cc->http_response_code == 409) {
            drupal_set_message(t('This campaign already exists (Code 409). Change subject if you want to create a new campaign'), 'warning');
        }
    endif;
}